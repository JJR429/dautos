import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    // === data
    currentUser: null,
    currentHeader: 0
  },
  getters: {
    getCurrentHeader(state) {
      return state.currentHeader;
    },
    // === computed ! get values on change
    getUsername(state) {
      // make call to API
      // call mutatios -> setUserData
      return state.currentUser.username;
    },
    getCurrentUserData(state) {
      return state.currentUser;
    },
    getIdColegio(state) {
      return state.currentUser.idcolegio;
    },
    getColegioLogo(state) {
      return state.currentUser.colegiologo;
    },
    getColegio(state) {
      return state.currentUser.colegio;
    },
    getIdGrado(state) {
      return state.currentUser.idgrado;
    },
    getGrado(state) {
      return state.currentUser.grado;
    }
  },
  actions: {
    setCurrentHeader(context, _type){
      context.commit("setCurrentHeader", _type);
    },
    // === methods ! Never update the State -> use to call API to fecht data
    setCurrentUser(context, _user) {
      context.commit("setCurrentUser", _user);
    },
    deleteCurrentUser({ commit }) {
      commit("deleteCurrentUser");
    },
    loadLocalStorage(context) {
      let currentUser = localStorage.getItem("currentUser");
      // console.log(JSON.parse(currentUser))
      if (currentUser) {
        try {
          currentUser = JSON.parse(currentUser);
        } catch (e) {
          currentUser = null;
        }
      }
      context.commit("mloadLocalStorage", currentUser);
    },
  },
  mutations: {
    setCurrentHeader(state, _type){
      state.currentHeader = _type;
    },
    // new on vuex ! use it for update the State -> set
    setCurrentUser(state, userdata) {
      localStorage.setItem("currentUser", userdata);
      state.currentUser = userdata;
    },
    deleteCurrentUser(state) {
      state.currentUser = null;
    },
    mloadLocalStorage(state, userdata) {
      state.currentUser = userdata;
    },
  }
});
