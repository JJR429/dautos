import auth from "@/router/auth";
import Vue from "vue";
import VueRouter from "vue-router";
import Home from "@/views/Home.vue";

// import store from "@/store";

Vue.use(VueRouter);

function requireAuth(to, from, next) {
  if (!auth.loggedIn()) {
    next({
      path: "/login",
      query: { redirect: to.fullPath }
    });
  } else {
    next();
  }
}

const routes = [
  {
    path: "/",
    name: "home",
    component: Home,
    props: true
  },
  {
    path: "/anuncios",
    name: "anuncios",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    props: true,
    component: () =>
      import(/* webpackChunkName: "Anuncios" */ "../views/Anuncios.vue")
  },
  {
    path: "/anuncios/:id_anuncio",
    name: "detalleAnuncio",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    props: true,
    component: () =>
      import(/* webpackChunkName: "DetalleAnuncio" */ "../views/DetalleAnuncio.vue")
  },
  {
    path: "/contacto",
    name: "contacto",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    props: true,
    component: () =>
      import(/* webpackChunkName: "contacto" */ "../views/Contacto.vue"),
  },
  {
    path: "/nosotros",
    name: "nosotros",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    props: true,
    component: () =>
      import(/* webpackChunkName: "nosotros" */ "../views/Nosotros.vue"),
  },
  {
    path: "/cuenta",
    name: "cuenta",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    props: true,
    component: () =>
      import(/* webpackChunkName: "chat" */ "../views/Cuenta.vue"),
    beforeEnter: requireAuth
  },
  {
    path: "/perfil",
    name: "perfil",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    props: true,
    component: () =>
      import(/* webpackChunkName: "chat" */ "../views/Perfil.vue"),
    beforeEnter: requireAuth
  },
  {
    path: "/anunciante/:username_anunciante",
    name: "anunciante",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    props: true,
    component: () =>
      import(/* webpackChunkName: "chat" */ "../views/Anunciante.vue")
  },
  {
    path: "/login",
    name: "login",
    component: () =>
      import(/* webPackChunkName: "Login" */ "../views/Login.vue")
  },
  {
    path: "/registrarse",
    name: "registrarse",
    component: () =>
      import(/* webPackChunkName: "Login" */ "../views/Registrarse.vue")
  },
  {
    path: "/terminos",
    name: "terminos",
    component: () =>
      import(/* webPackChunkName: "Login" */ "../views/Terminos.vue")
  },
  {
    path: "/publicar",
    name: "publicar",
    component: () =>
      import(/* webPackChunkName: "Login" */ "../views/Publicar.vue"),
    beforeEnter: requireAuth
  },
  {
    path: "/logout",
    beforeEnter(to, from, next) {
      auth.logout();
      next("/");
    }
  },
  {
    path: "*",
    name: "notfound",
    props: true,
    component: () =>
      import(/* webpackChunkName: "not-found" */ "../views/404.vue")
  }
];

const router = new VueRouter({
  mode: "history",
  linkExactActiveClass: "playroom-active-link",
  routes
});

export default router;
